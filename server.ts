import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import { randomBytes } from 'crypto';
import { upsertDb } from './lib/databases/index';
import cors from 'cors';
import { writeFileSync } from 'fs';
import * as bodyParser from 'body-parser';
const app = express();
const port = process.env.API_PORT || 3001;
import {join} from "path";
const winston = require('winston');
global.logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
        new winston.transports.File({ filename: './logs/combined.log' }),
    ],
});

if (process.env.NODE_ENV !== 'production') {
    global.logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}
const origins = [`localhost`, `http://localhost:3000`, `${process.env.API_HOSTNAME}`, `${process.env.API_HOSTNAME}:${process.env.UI_PORT}`];
app.use(cors({
    credentials: true,
    "origin": function (origin, callback) {
        if (typeof origin === 'undefined') {
            return callback(null, true);
        }
        if (origins.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    },
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


import setup from './lib/passport';
setup(app);
app.use('/', express.static(join(__dirname, 'client')))


if (process.env.AUTH_METHOD === 'demo') {
    app.get('/login', async (req, res) => {
        let {query} = req;
        let password = randomBytes(10).toString('hex');
        if (!query.username) {
            return  res.send(`Please specify a username`);
        }
        let user = await upsertDb(query.username, password);
        res.send(`Login with username: ${req.query.username} & password: ${password}`)
    })
}

app.post('/api/databases/:id', async (req, res) => {
    let databases = require('./databases.json');
    let match = databases.find(d => d.id === req.params.id);
    if (!match) {
        return res.json({
            error: "Error: Database not found"
        }).status(404);
    }

    let credentials;
    try {
        credentials = await upsertDb(req, match);
    }
    catch (e: any) {
        return res.json({
            error: e.toString()
        }).status(500);
    }

    res.json(credentials);
});

app.get('/api/databases', (req, res) => {
    let databases = require('./databases.json');
    const filtered  = databases.map((data: object) => {
        return {
            id: data.id,
            hostname: data.hostname,
            port: data.port,
            type: data.type,
            database: data.database,
            name: data.name,
            id: data.id
        }
    });
    res.json(filtered);
});

let providerMap = {
    google: `${process.env.API_HOSTNAME}:${process.env.API_PORT}/api/login/google`
}
app.get('/api/provider', (req, res) => {
    res.json({
        provider: process.env.AUTH_METHOD,
        location: providerMap[process.env.AUTH_METHOD]
    });
});


app.get('/api/session', function(req, res, next) {
    res.json(req.session);
});

app.listen(port, () => {
    console.log(`db-proxy web server running at ${process.env.API_HOSTNAME}:${port}`)
})


function setupDatabases() {
    try {
        let databases = require('./databases.json');
        let changes = false;
        for (let database of databases) {
            if (!database.id) {
                changes = true;
                database.id = randomBytes( 16).toString('hex');
            }
        }
        if (changes) {
            writeFileSync('./databases.json', JSON.stringify(databases));
        }
    } catch (err) {
        console.error('No databases.json found');
    }

}
setupDatabases();