# Setting up Google OAuth with the SSO-Postgres agent

## Step 1: Get to the right page
* Login to GCP console
* Search for "OAuth" service
* Choose Credentials, APIs & Services

## Step 2: Create a OAuth client ID
* At the top of the console, pick the "+ Create Credentials" button
![](google/create_credentials.png)
* Choose "OAuth client ID" as the type
* On the next page choose "Web application" under application type
* Should see the below page
![](google/create_client_id.png)

You need to configure the following:
* Name - Choose to fit your coding standards
* Authorized Javascript Origins
** This is the hostname for the website portal, including port if not on 443
* Authorized redirect URLs
** This is where the browser will be allowed to redirect to once logging into identity provider is completed
* Needs to be https://\<hostname\>/api/oauth2/redirect/accounts.google.com
