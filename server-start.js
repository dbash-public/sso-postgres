#!/usr/bin/env node

const fs = require('fs');
const childProcess = require('child_process');
const { spawn, execSync } = require('child_process');

function execSyncSafe(command) {
    try {
        execSync(command);

    } catch (err) {
        console.log(err.stdout.toString());
    }
}

if (!fs.existsSync("./node_modules")) {
    execSync("npm i");
}

if (!fs.existsSync("./client/node_modules")) {
    execSync("cd client && npm i");
}

execSyncSafe("cd build && rm -rf *");
execSyncSafe("npm run build-api");
execSyncSafe("cd client && npm run build");
execSyncSafe("cd client && cp -r build ../build/client");
console.log("Starting server on port 3001 ...")
// execSyncSafe("cd build && node server.js");

const server = spawn('cd build && node server.js');

server.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
})

server.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
})
