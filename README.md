# sso-postgres

* SSO-Postgres provisions user accounts for a Postgres database using your central identity provider(s).

* SSO-Postgres does not require an enterprise license for any database 

* SSO-Postgres utilizes JIT user creation when a user requests access.

* SSO-Postgres supports multiple database types, consolidating them under a single identity provider.

## SSO Example

![](https://gitlab.com/dbash-public/sso-postgres/-/raw/main/docs/media/workflow.gif)

## Supported Databases

* Postgres
* Aurora Postgres
* MySQL
* Mongodb

## Supported Identity Providers

* AzureAD
* [Google Oauth](https://gitlab.com/dbash-public/sso-postgres/-/tree/main/docs/identity_providers/google.md)
* Auth0
* Facebook
* [Any strategy supported by PassportJS](https://www.passportjs.org/packages/) 

## Configuration

_SSO-Postgres uses environment variables for configuration (set with an .env file at the server's root directory)._

Environment variables:

* PG_DEFAULT_PERMISSIONS="all privileges"
* G_OAUTH_API_KEY=""
* G_OAUTH_SECRET=""
* AUTH_METHOD=<your selected identity provider e.g. google, azuread, facebook>
* HOST_NAME=localhost
* SESSION_SECRET=<session secret to use>
* API_HOSTNAME=localhost
* UI_HOSTNAME=localhost
* API_PORT=3001
* UI_PORT=3001 // can host UI in a CDN technically
