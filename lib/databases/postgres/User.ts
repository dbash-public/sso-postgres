const { Pool, Client } = require('pg');
// Postgres does not support parameterized queries in CREATE user / ALTER user
const escape = require('pg-escape');
const env = process.env;

export class Postgres {
    private pool: Pool;
    constructor(database: object) {
        this.pool = new Pool({
            user: database.username,
            host: database.host,
            database: database.database,
            password: database.password,
            port: database.port,
        });
    }

    async upsertUser(username: string, password: string) {
        // The below query uses a prepared statement, the rest in this chain cannot unfortunately
        // https://www.postgresql.org/message-id/925094a20608222109s438a5b41g2886f41e9ddf7417@mail.gmail.com
        const user = await this.getUser(username);
        username = escape(username);
        password = escape(password);
        if (!user) {
            return await this.createUser(username, password);
        }

        return this.updateUser(username, password);
    }

    async getUser(username: string) {
        const query = `SELECT usename, usecreatedb, usesuper FROM pg_catalog.pg_user WHERE usename = $1 LIMIT 1;`;
        const result =  await this.pool.query(query, [username]);
        if (result.rows.length === 0) {
            return false;
        }
        return result.rows[0];
    }

    async createUser(username: string, password: string) {
        const query = `CREATE USER ${username}`;
        const result =  await this.pool.query(query);
        await this.updateUser(username, password);
        return result;
    }

    async updateUser(username: string, password: string) {
        console.log('update user');
        const query = `alter user ${username} with encrypted password '${password}';`;
        const alterUser = await this.pool.query(query);
        const permissions = `GRANT ${this.getPermissions()} ON DATABASE ${env.PG_DATABASE} TO ${username};`
        const alterPermissions =  await this.pool.query(permissions);

        return [alterUser, alterPermissions];
    }

    getPermissions() {
        if (!env.PG_DEFAULT_PERMISSIONS) {
            return 'SELECT';
        }

        return env.PG_DEFAULT_PERMISSIONS;
    }
}
