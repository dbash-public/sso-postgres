import {Postgres} from "./postgres/User";
import {randomBytes} from "crypto";
export async function upsertDb(req: Request, database: object) {
    const session = req.session;
    const username = session.passport.user.username;
    let password = randomBytes(16).toString('hex');

    if (database.type === 'pg') {
        let pg = new Postgres(database);
        await pg.upsertUser(username, password);
    }

    return {
        username, password
    }

}