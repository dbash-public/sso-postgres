import {Application} from "express";
import setupPassport from "./Google";
import session from 'express-session';
const FileStore = require('session-file-store')(session);
const fileStoreOptions = {
    path: process.env.SESSION_STORAGE || './mount/sessions',
    ttl: 3600
};

export default function setup(app: Application) {
    let options = {
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: true,
        store: new FileStore(fileStoreOptions),
        rolling: true,
        cookie: {
            secure: false,
            maxAge: 600000,
            sameSite: true,
            httpOnly: false
        },
    }

    if (app.get('env') === 'production') {
        app.set('trust proxy', 1) // trust first proxy
        options.cookie.secure = true // serve secure cookies
    }
    app.use(session(options));

    switch (process.env.AUTH_METHOD) {
        case 'google': return setupPassport(app);
        default: new Error(`Unsupported auth method: ${process.env.AUTH_METHOD}`);
    }
}