import React, { Fragment, useState, useEffect } from 'react';
import {Row, Hero, Credentials, Databases, Provider, APIState} from 'components/lib';

export function Home(props) {

    const [progress, setProgress] = useState('awaitingLogin');
    const [session, setSession] = useState(null);
    const [provider, setProvider] = useState(null);
    const [database, setDatabase] = useState(null);
    const [credentials, setCredentials] = useState(null);
    const [loginState, setLoginState] = useState("");
    const [error, setError] = useState("");

    useEffect(() => {
        APIState(setSession,  '/api/session');
        APIState(setProvider,  '/api/provider');
    }, []);

    useEffect(() => {
        if (credentials?.data?.username) {
            setError("");
            setProgress('showCredentials');
        } else if (session?.data?.passport) {
            setLoginState(`Logged in as ${firstToUpper(session.data.passport.user.username)}`)
            setProgress('selectDatabase');
        } else if (provider?.data?.provider) {
            setLoginState(`Login with ${firstToUpper(provider.data.provider)}`)
        }

        if (credentials?.data?.error) {
            setError(credentials.data.error);
        }
    }, [credentials, session, database, provider]);

    function firstToUpper(wordToUpper) {
        if (wordToUpper === null || wordToUpper.length === 0) return "";
        return `${wordToUpper[0].toUpperCase()}${wordToUpper.slice(1)}`;
    }

    let content;
    switch (progress) {
        case 'awaitingLogin':
            content = <Row title='Step 1: Confirm Your Identity' subTitle={error} color='white'><Provider /></Row>;
            break;
        case 'selectDatabase':
            content = <Row title='Step 2: Select a Database' subTitle={error} color='white'><Databases setCredentials={setCredentials} setDatabase={setDatabase} /></Row>;
            break;
        case 'showCredentials':
            content = <Row title={`Step 3: Your Credentials for ${database.name}`} subTitle={error} color='white'><Credentials credentials={credentials.data} database={database} setProgress={setProgress} setCredentials={setCredentials} /></Row>;
            break;
        default:
            return null;
    }

    return(
    <Fragment>
        <Hero
            title="Database Single Sign-on"
            tagline={`${loginState}`}
            />
        {content}
    </Fragment>
  );
}
