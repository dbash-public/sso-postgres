/***
*
*   TERMS & CONDITIONS
*   Boilerplate terms & conditions – replace the values
*   inside [] with your own text
*
**********/

import React from 'react';
import Faq from 'react-faq-component';
import { Article } from 'components/lib';
const data = {
  rows: [
    {
      title: "I am getting an error, who should I contact?",
      content: "Please contact your IT help desk or direct manager"
    },
    {
      title: "What permissions do I have?",
      content: "Your administrator configures your permissions, contact them to have any changes made."
    }
  ]
}
export function FAQ(){

  return(

    <Article>

      <h1>FAQ</h1>

      <div>
        <Faq data={data}/>
      </div>

    </Article>
  )
}
