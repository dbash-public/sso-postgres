import { OnboardingView } from 'views/onboarding/onboarding';
import { Filter } from 'views/filter';
import { Reporting } from 'views/reporting';

const Routes = [
  {
    path: '/welcome',
    view: OnboardingView,
    layout: 'onboarding',
    permission: 'user',
    title: 'Welcome'
  },
  {
    path: '/filter',
    view: Filter,
    layout: 'app',
    permission: 'user',
    title: 'Filter'
  },
  {
    path: '/reporting',
    view: Reporting,
    layout: 'app',
    permission: 'user',
    title: 'Reporting'
  },
]

export default Routes;
