/***
*
*   HOVER NAV
*   Reveals a nav when the user hovers over a hotspot
*   Items are rendered as children, revealed is achieved with CSS
*
*   PROPS
*   dark: boolean to set color (default: light)
*   label: the hotspot text
*   align: left or right
*
**********/

import React, { useState } from 'react';
import ClassNames from 'classnames';
import { Animate, Icon } from 'components/lib';
import Style from './hover.module.scss';

export function HoverNav(props){

  // state
  const [open, setOpen] = useState(false);

  // style
  const css = ClassNames([

    Style.hoverNav,
    props.className,
    props.dark && Style.dark,
    props.align && Style[props.align]

  ]);

  return (
    <div className={ css }
      onMouseEnter={ e => setOpen(true)}
      onMouseLeave={ e => setOpen(false)}>

      <span className='label'>
        { props.icon && <Icon image={ props.icon } size={ 15 } className={ Style.icon }/>}
        { props.label }
      </span>

      { open && props.children?.length && 
        <Animate type='slidedown' timeout={ 50 }>
        <nav className='dropdown'>
          { props.children }
        </nav>
        </Animate>
      }

    </div>
  );
}
