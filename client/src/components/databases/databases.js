import { Fragment, useState, useEffect } from 'react';
import { APIState, Card, Button, Table } from 'components/lib';

export function Databases(props){

    const [databases, setDatabases] = useState({ data: null, loading: false });
    const [tableRows, setTableRows] = useState([]);

    useEffect(() => {
        APIState(setDatabases,  '/api/databases');
    }, []);

    useEffect(() => {
        if (!databases.data) return;

        setTableRows(databases.data.map( (database) => {
            return {
                "Name": database.name,
                "Hostname": database.hostname,
                "Database": database.database,
                "Type": database.type,
                "Action":  <SetAction database={database} />,
            }
        }));
    }, [databases]);

    async function selectDatabase(database) {
        props.setDatabase(database);
        await APIState(props.setCredentials,  `/api/databases/${database.id}`, 'post');
    }

    function SetAction(props) {
        return <Button
            iconButton = { true }
            icon = 'database'
            size = { 32 }
            text = { `Select ${props.database.name}` }
            action ={ () => selectDatabase(props.database) }
        />
    }

    return(
      <Fragment>
          <Card>
              <Table
                  className='restrict-width'
                  data={ tableRows }
                  loading={ databases?.loading }
          />
          </Card>
      </Fragment>
  );

}
