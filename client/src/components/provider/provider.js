import { useEffect, useState } from 'react';
import { APIState, Button } from 'components/lib';

export function Provider(props){

    const [provider, setProvider] = useState({ data: null, loading: false });

    async function login() {
        await APIState(setProvider,  '/api/provider');
    }

    useEffect(() => {
        if (provider?.data?.location) {
            window.location = provider.data.location;
        }
    }, [provider]);

    return(
      <div style={{textAlign: "center"}}>
          <Button
              iconButton = { true }
              icon = 'log-in'
              size = { 32 }
              text = { `Login` }
              action ={ () => login() }
          />
      </div>
  );

}
