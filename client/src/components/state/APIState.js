import Axios from 'axios';
export async function APIState(setState, url, method, formatter){

  try {

    if (!url){
      setState({ data: null, loading: false });
      return;
    }

    setState({ loading: true });
    const res = await Axios({
      url: localStorage.getItem('apiPath') + url,
      method: method || 'get',
      withCredentials: true
    })

    if (formatter && res?.data) {
      res.data = formatter(res.data);
    }

    setState({ data: res?.data, loading: false });

  } catch (err){
    console.error(err);
  }

}
