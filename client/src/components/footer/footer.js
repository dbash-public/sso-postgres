/***
*
*   FOOTER (homepage)
*   Static homepage footer
*
**********/

import React from 'react';
import { Animate, Row, Content, Link } from 'components/lib'
import Style from './footer.module.scss';

export function Footer(props){

  return (
    <Animate type='slideup'>
      <footer className={ Style.footer }>
        <Row>
          <Content>

            <nav>
            <Link url='/' text='Home'/>
            </nav>

            <address>dba.sh - The SecOps Platform</address>

          </Content>
        </Row>
      </footer>
    </Animate>
  );
}
